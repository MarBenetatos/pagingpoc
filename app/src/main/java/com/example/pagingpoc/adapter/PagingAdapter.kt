package com.example.pagingpoc.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.pagingpoc.R
import com.example.pagingpoc.databinding.RecyclerItemViewBinding
import com.example.pagingpoc.api.repository.dao.Result
import com.example.pagingpoc.di.Finals

class PagingAdapter(private val listener: OnItemClickListener) :
    PagingDataAdapter<Result, PagingAdapter.MovieViewHolder>(MOVIE_COMPARATOR) {

    inner class MovieViewHolder(private val binding: RecyclerItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                // in case I add animations
                if (bindingAdapterPosition != RecyclerView.NO_POSITION) {
                    getItem(bindingAdapterPosition)?.let {
                        listener.itemClicked(it)
                    }
                }
            }
        }

        fun bind(movies: Result) {
            binding.apply {
                text.text = movies.title
                rating.text=movies.vote_average.toString()
                Glide.with(itemView).load(Finals.BASE_URL_PHOTOS.plus(movies.backdrop_path))
                    .centerCrop()
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .error(R.drawable.ic_error_image).into(imageView)
            }
        }
    }

    interface OnItemClickListener {
        fun itemClicked(movie: Result)
    }

    companion object {
        private val MOVIE_COMPARATOR = object : DiffUtil.ItemCallback<Result>() {
            override fun areItemsTheSame(oldItem: Result, newItem: Result): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Result, newItem: Result) = oldItem == newItem
        }
    }

    //adapter
    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val currentItem = getItem(position)
        currentItem?.let {
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding =
            RecyclerItemViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return MovieViewHolder(binding)
    }
}