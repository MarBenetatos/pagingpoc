package com.example.pagingpoc.data

import com.example.pagingpoc.api.repository.dao.Result

data class RecyclerModel(
    val result: List<Result>
)