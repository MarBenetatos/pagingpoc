package com.example.pagingpoc.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.pagingpoc.api.Services
import com.example.pagingpoc.di.Finals
import com.example.pagingpoc.api.repository.dao.Result
import retrofit2.HttpException
import java.io.IOException

class RecyclerPagingSource(
    private val api: Services,
    private val query: String
) : PagingSource<Int, Result>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Result> {
        val position : Int = params.key ?: Finals.STARTING_PAGE
        return try {

        val response =api.getMovies( query , position , Finals.API_KEY)

            LoadResult.Page(
                data = response.body()!!.results,
                prevKey = if (position ==  Finals.STARTING_PAGE) null else position - 1,
                nextKey = if (response.body()!!.results.isEmpty()) null else position + 1
            )
        }catch (exception: IOException) {
        LoadResult.Error(exception)
    } catch (exception: HttpException) {
        LoadResult.Error(exception)
    }
    }

    override fun getRefreshKey(state: PagingState<Int, Result>): Int? {
        TODO("Not yet implemented")
    }
}