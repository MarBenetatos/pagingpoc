package com.example.pagingpoc.ui

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.pagingpoc.R
import com.example.pagingpoc.databinding.DetailsFragmentBinding
import com.example.pagingpoc.di.Finals

class DetailsFragment : Fragment(R.layout.details_fragment) {

    private val args by navArgs<DetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = DetailsFragmentBinding.bind(view)

        binding.apply {
            val model =args.model
            Glide.with(this@DetailsFragment)
                .load(Finals.BASE_URL_PHOTOS.plus(model.backdrop_path))
                .error(R.drawable.ic_error)
                .listener(object  : RequestListener<Drawable>{
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                       progressBar.isVisible=false
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar.isVisible=false
                        title.isVisible=true
                        rating.isVisible=true
                        text.isVisible=true
                        return false
                    }
                })
                .into(imageView)

            title.text = model.title
            rating.text = model.vote_average.toString()
            text.text = model.overview

        }
    }
}