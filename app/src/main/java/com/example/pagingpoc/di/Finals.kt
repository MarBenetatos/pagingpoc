package com.example.pagingpoc.di

object Finals {
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val BASE_URL_PHOTOS = "https://image.tmdb.org/t/p/w500"
    const val STARTING_PAGE=1
    const val API_KEY="2810b46c0fe82e2e7eb43466581d495f"
    const val DEFAULT_QUERY_VALUE="a"
    const val ENCODE="utf-8"
}