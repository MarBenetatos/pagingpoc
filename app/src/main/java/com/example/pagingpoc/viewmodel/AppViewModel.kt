package com.example.pagingpoc.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.example.pagingpoc.api.repository.Repo
import com.example.pagingpoc.di.Finals
import dagger.hilt.android.lifecycle.HiltViewModel
import java.net.URLEncoder
import javax.inject.Inject

@HiltViewModel
class AppViewModel @Inject  constructor(
    private val repo: Repo,
) : ViewModel(){

    private val currentQuery = MutableLiveData(Finals.DEFAULT_QUERY_VALUE)

    val resultData= currentQuery.switchMap {
        repo.getMoviesSearch(URLEncoder.encode(it,Finals.ENCODE)).cachedIn(viewModelScope)
}

    fun searchMovies(query: String){
        currentQuery.value=query
    }
}