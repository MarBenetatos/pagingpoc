package com.example.pagingpoc.api.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.example.pagingpoc.api.Services
import com.example.pagingpoc.data.RecyclerPagingSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repo @Inject
constructor(private val service: Services) {

    fun getMoviesSearch(query : String) = Pager(
        config = PagingConfig(
            pageSize = 20,
            maxSize = 100,
            enablePlaceholders = false
        ),
        pagingSourceFactory = { RecyclerPagingSource(service,query) }
    ).liveData
}