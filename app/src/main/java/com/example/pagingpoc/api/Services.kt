package com.example.pagingpoc.api

import com.example.pagingpoc.api.repository.dao.SearchMoviesDao
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Services {

    @GET("search/movie")
    suspend fun getMovies(
        @Query("query") query : String,
        @Query("page") page : Int,
        @Query("api_key") apiKey : String
    ): Response<SearchMoviesDao>
}